const Product = require("../models/Product");
const express = require('express');

class productController {
  async createProduct(req, res) {
    try {
        const { title, quantity, measurementUnit, records } = req.body;
        const product = new Product({
            title: title,
            quantity: quantity,
            measurementUnit: measurementUnit,
            records: records
        });
        await product.save();
        res.status(201).json(product);
    } catch (err) {
      console.log(err);
      res.status(400).json({ message: err });
    }
  }

  async getProducts(req, res) {
    try{
        const products = await Product.find({});    
        const reverseProduct = products.reverse();
        res.json(reverseProduct);
    }catch(err){
        console.log(err);
        res.status(400).json({ message: err });
    }
  }
  
  async getProductById(req, res) {
    try {
        const productId = req.params.id;
        const product = await Product.findById(productId);
        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }
        res.json(product);
    } catch (err) {
        console.error(err);
        res.status(400).json({ message: err.message });
    }
  }

  async updateProduct(req, res) {
    try {
        const productId = req.params.id;
        const { title, quantity, measurementUnit, records } = req.body;
        const updatedProduct = await Product.findByIdAndUpdate(productId, {
            title,
            quantity,
            measurementUnit,
            records
        }, { new: true });

        if (!updatedProduct) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.json(updatedProduct);
    } catch (err) {
        console.error(err);
        res.status(400).json({ message: err.message });
    }
  }

  async deleteProduct (req, res){
    try {
        const productId = req.params.id;
        const deletedProduct = await Product.findByIdAndDelete(productId);
        if (!deletedProduct) {
            return res.status(400).json({ message: 'Product not found' });
        }
        res.json({ message: 'Product deleted successfully' });
    } catch (err) {
        console.error(err);
        res.status(400).json({ message: err.message });
    }
  }
  
}

module.exports = new productController();