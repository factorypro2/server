const User = require("../models/User");
const express = require('express');
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { secret } = require("../config");


const generateAccestoken = (id, roles) => {
  const payload = {
    id,
    roles,
  };
  return jwt.sign(payload, secret, { expiresIn: "1h" });
};

class authController {
  async registerUser(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      const { username, firstName, lastName, middleName, dateOfBirth, profileImageUrl, password, roles } = req.body;
      const candidate = await User.findOne({ username });
      if (candidate) {
        return res.status(400).json({ message: "User already exists" });
      }
      const salt = bcrypt.genSaltSync(10);
      const hashPassword = bcrypt.hashSync(password, salt);
      const user = new User({
        username: username,
        firstName: firstName,
        lastName: lastName,
        middleName: middleName,
        dateOfBirth: dateOfBirth,
        profileImageUrl: profileImageUrl,
        password: hashPassword,
        roles: roles,
      });
      await user.save();
      
      // Login
      const createdUser = await User.findOne({ username: user.username });
      const token = generateAccestoken(createdUser._id, createdUser.roles);
      createdUser.lastLogin = new Date();
      createdUser.token = token;
      await createdUser.save();
      const result = { user: user };
      return res.json(result);
    } catch (err) {
      console.log(err);
      res.status(400).json({ message: err });
    }
  }

  async changePassword(req, res){
    const {userId, password, newPassword} = req.body;
    const user = await User.findById(userId);
    console.log(user)
    const validPassword = bcrypt.compareSync(password, user.password);
    if (!validPassword) {
      return res.status(400).json({ message: "Invalid password" });
    }

    const salt = bcrypt.genSaltSync(10);
    const hashPassword = bcrypt.hashSync(newPassword, salt);

    user.password = hashPassword;
    await user.save();
    res.status(200).json({ message: "Password Updated" });
  }

  async loginUser(req, res) {
    try {
      const { username, password } = req.body;
      const user = await User.findOne({ username: username });
      if (!user) {
        return res.status(400).json({ message: "User not found" });
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({ message: "Invalid password" });
      }
      const token = generateAccestoken(user._id, user.roles);
      user.lastLogin = Date.now();
      user.token = token;
      await user.save();
      return res.json(user);
    } catch (err) {
      console.log(err);
      res.status(400).json({ message: "Login Error" });
    }
  }

  async updateUserById(req, res) {
    try {
      const id = req.params.id;
      const { username, firstName, lastName, middleName, dateOfBirth, roles } = req.body;
      const user = await User.findOne({ _id: id });
      if (!user) {
        return res.status(400).json({ message: "User not found" });
      }
      user.firstName = firstName;
      user.lastName = lastName;
      user.middleName = middleName;
      user.dateOfBirth = dateOfBirth;
      user.username = username;
      user.roles = roles;
      await user.save();
      res.status(200).json({ message: "User updated" });
    } catch (err) {
      console.log(err);
    }
  }
  async getAllUsers(req, res) {
    try {
      const users = await User.find({});
      const reversedUsers = users.reverse();
      res.json(reversedUsers);
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "Internal server error" });
    }
  }
  async getUserById(req, res) {
    try {
      const id = req.params.id;
      const user = await User.findOne({ _id: id });
      if (!user) {
        return res.status(400).json({ message: "User not found" });
      }
      res.status(200).json(user);
    } catch (err) {
      console.log(err);
    }
  }

  async getUserByUsername(req, res) {
    try {
      const username = req.params.username;
      const user = await User.findOne({ username: username });
      if (!user) {
        return res.status(400).json({ message: "User not found" });
      }
      res.status(200).json(user);
    } catch (err) {
      console.log(err);
    }
  }
  
  async deleteUser(req, res) {
    try {
      const id = req.params.id;
      const result = await User.deleteOne({ _id: id });

      if (result.deletedCount === 0) {
        return res.status(404).json({ message: "User not found" });
      }

      res.status(204).json({ message: "User deleted" });
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: "Internal Server Error" });
    }
  }
}

module.exports = new authController();