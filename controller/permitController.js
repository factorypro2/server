const Permit = require("../models/Permit");
const User = require('../models/User')
const express = require('express');


class permitController{
    async createPermit (req, res){
        try{
            const {title, type, typeImageUrl, description, location, startDate, endDate, numberOfPersons, equipments, signatures, state, userId} = req.body;
            const user = await User.findOne({ _id: userId }); 
            const permit = new Permit({
                title: title, 
                type: type, 
                typeImageUrl: typeImageUrl, 
                description: description, 
                location: location,
                startDate: startDate, 
                endDate: endDate, 
                numberOfPersons: numberOfPersons, 
                equipments: equipments, 
                signatures: signatures, 
                state: state, 
                userId: userId,
                username: user.username
              });
              await permit.save();
              return res.json(permit);
        }catch(err){
            console.log(err);
            res.status(400).json({ message: err });
        }
    }   

    async getAllPermits(req, res) {
        try {
          const permits = await Permit.find({});
          res.json(permits);
        } catch (err) {
          console.log(err);
        }
    }
    
    async getPermit(req, res){
      try{
        const result = [];
        const permit = await Permit.findById(req.params.id);
        result.unshift(permit);
        res.json(result)
      }catch(err){
        console.log(err);
      }
    }

    async signPermit(req, res){
        try{
          // (permitId, signatureTitle, signerId, newState)
            const permitId = req.body.permitId;
            const signature = req.body.signature;
            const newState = req.body.state;
            console.log(permitId, signature, newState)
            const permit = await Permit.findOne({ _id: permitId });
            permit.state = newState;
            permit.signatures.unshift(signature);
            await permit.save();
            return res.json(permit);
        }
        catch(err){
            console.error(err);
            res.status(500).json({ message: "Internal Server Error" });
        }
    }

    async deletePermit(req, res) {
        try {
          const id = req.body.id;
          const result = await Permit.deleteOne({ _id: id });
    
          if (result.deletedCount === 0) {
            return res.status(404).json({ message: "User not found" });
          }else{
              res.status(204).json({ message: "User deleted" });
          }    
        } catch (err) {
          console.error(err);
          res.status(500).json({ message: "Internal Server Error" });
        }
      }

}

module.exports = new permitController();