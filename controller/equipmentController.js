const Equipment = require("../models/Equipment");
const express = require('express');


class permitController{
    async createEquipment (req, res){
        try{
            const {code, title, description, quantity} = req.body;
            const equipment = new Equipment({
                code: code, 
                title: title, 
                description: description, 
                quantity: quantity
              });
              await equipment.save();
              return res.json(equipment);
        }catch(err){
            console.log(err);
            res.status(400).json({ message: err });
        }
    }   

    async getAllEquipments(req, res) {
        try {
          const equipments = await Equipment.find({});
          res.json(equipments);
        } catch (err) {
          console.log(err);
        }
    }
    
    async getEquipment(req, res){
      try{
        const result = [];
        const equipment = await Equipment.findById(req.params.id);
        result.unshift(equipment);
        res.json(result)
      }catch(err){
        console.log(err);
      }
    }

    async editEquipment(req, res) {
        try {
            const { id, code, title, description, quantity } = req.body;
            const equipment = await Equipment.findById(id);
            if (!equipment) {
                return res.status(404).json({ message: "Equipment not found" });
            }
    
            equipment.code = code;
            equipment.title = title;
            equipment.description = description;
            equipment.quantity = quantity;
    
            await equipment.save();
    
            return res.json(equipment);
        } catch (err) {
            console.log(err);
            res.status(400).json({ message: "Failed to edit equipment", error: err });
        }
    }

    async deleteEquipment(req, res) {
        try {
          const id = req.body.id;
          const result = await Equipment.deleteOne({ _id: id });
    
          if (result.deletedCount === 0) {
            return res.status(404).json({ message: "Equipment not found" });
          }else{
              res.status(204).json({ message: "Equipment deleted" });
          }    
        } catch (err) {
          console.error(err);
          res.status(500).json({ message: "Internal Server Error" });
        }
      }

}

module.exports = new permitController();