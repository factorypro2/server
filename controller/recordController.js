const Product = require("../models/Product");
const Record = require("../models/Record");
const User = require("../models/User");
const express = require('express');

class recordController {
  async createRecord(req, res) {
    try {
        const { productId, type, quantity, authorId } = req.body;
        const user = await User.findOne({ _id: authorId });
        if (!user) {
            return res.status(400).json({ message: "User not found" });
        }
        const product = await Product.findById(productId);
        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }
        const previousValue = product.quantity
        let newValue = 0

        if(type == "add"){
            newValue = previousValue + quantity
            product.quantity = newValue
        }
        if(type == "sub"){
            newValue = previousValue - quantity
            product.quantity = newValue
        }

        const date = new Date();


        const record = new Record({
            productId: productId,
            type: type,
            quantity: quantity,
            author: user.username,
            previousValue: previousValue,
            newValue: newValue,
            date: date
        })

        product.records.unshift(record)
        await record.save();
        await product.save();
        res.status(201).json(record);
    } catch (err) {
      console.log(err);
      res.status(400).json({ message: err });
    }
  }

  async deleteRecord(req, res){
    try {
        const recordId = req.params.id;
    
        const record = await Record.findById(recordId);
        if (!record) {
            return res.status(400).json({ message: 'Record not found' });
        }
    
        const product = await Product.findById(record.productId);
        if (!product) {
            return res.status(400).json({ message: 'Product not found' });
        }
    
        const filteredRecords = product.records.filter(prodRecord => prodRecord._id.toString() !== recordId);
        product.records = filteredRecords;
    
        await product.save();
    
        const deletedRecord = await Record.findByIdAndDelete(recordId);
        if (!deletedRecord) {
            return res.status(400).json({ message: 'Record not found' });
        }
    
        res.status(200).json({ message: 'Record deleted successfully' });
    } catch (err) {
        console.error(err);
        res.status(400).json({ message: err.message });
    }
  }
  
}

module.exports = new recordController();