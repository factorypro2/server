const Order = require("../models/Order");
const OrderId = require("../models/OrderId");
const Record = require("../models/Record");
const User = require("../models/User")
const Product = require("../models/Product");

const express = require('express');

class orderController {
  async createOrder(req, res) {
    try {
        const {orderer, authorId, productId, productTitle, quantity, remarks} = req.body;
        const orderId = await OrderId.findById('65ff425fb1289ae33cb8fae9')
        const lastOrderId = orderId.lastOrderId
        const orderTitle = `ORD-0000${lastOrderId + 1}`
        const date = new Date();
        
        const updatedOrderId = await OrderId.findByIdAndUpdate("65ff425fb1289ae33cb8fae9", {
            lastOrderId: lastOrderId + 1
        }, { new: true });


        // Create Record Based on the Order
        const user = await User.findOne({ _id: authorId });
        if (!user) {
            return res.status(400).json({ message: "User not found" });
        }
        const product = await Product.findById(productId);
        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }
        const previousValue = product.quantity
        let newValue = 0

        newValue = previousValue - quantity
        product.quantity = newValue



        const record = new Record({
            productId: productId,
            type: 'sub',
            quantity: quantity,
            author: user.username,
            previousValue: previousValue,
            newValue: newValue,
            date: date,
            orderTitle: orderTitle
        })

        const order = new Order({
            state: "Pending",
            orderTitle: orderTitle,
            orderer: orderer,
            authorId: authorId,
            productId: productId,
            productTitle: product.title,
            quantity: quantity,
            remarks: remarks,
            date: date
        })

        product.records.unshift(record)


        await record.save();
        await product.save();
        await order.save();       
        res.status(201).json(order);
    } catch (err) {
        console.log(err);
        res.status(400).json({ message: err });
    }
  }
  async getAllOrders(req, res){
    try{
        const orders = await Order.find({});    
        const reverseOrders = orders.reverse();
        res.json(reverseOrders);
    }catch(err){
        console.log(err);
        res.status(400).json({ message: err });
    }
  }

  async getOrderById(req, res) {
    try {
        const orderId = req.params.id;
        const order = await Order.findById(orderId);
        if (!order) {
            return res.status(404).json({ message: 'Order not found' });
        }
        res.json(order);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
  }

  async changeState(req, res){
    try{
        const {orderId, newState} = req.body;
        const updatedOrderState = await Order.findByIdAndUpdate(orderId, {
            state: newState
        }, { new: true });
        res.json({message: "Updated"})
    }catch(err){
        console.log(err);
        res.status(400).json({ message: err });
    }
  }
}

module.exports = new orderController();