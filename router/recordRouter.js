const recordController = require("../controller/recordController")
const Router = require("express");
const router = new Router();

router.post("/create", recordController.createRecord);
// router.get("/all", recordController.getProducts);
// router.get("/:id", recordController.getProductById);
// router.put("/edit/:id", recordController.updateProduct);
router.delete('/delete/:id', recordController.deleteRecord)



module.exports = router;

