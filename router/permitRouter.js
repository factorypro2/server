const permitController = require("../controller/permitController")
const Router = require("express");
const router = new Router();

//Permit Routes
router.get("/", permitController.getAllPermits);
router.post("/create", permitController.createPermit);
router.get("/:id", permitController.getPermit)
router.delete("/delete", permitController.deletePermit);
router.put("/sign", permitController.signPermit);

module.exports = router;
