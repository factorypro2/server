const equipmentController = require("../controller/equipmentController")
const Router = require("express");
const router = new Router();

//Permit Routes
router.get("/", equipmentController.getAllEquipments);
router.post("/create", equipmentController.createEquipment);
router.get("/:id", equipmentController.getEquipment)
router.delete("/delete", equipmentController.deleteEquipment);
router.put("/edit", equipmentController.editEquipment);


module.exports = router;
