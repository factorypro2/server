const productController = require("../controller/productController")
const Router = require("express");
const router = new Router();

router.post("/create", productController.createProduct);
router.get("/all", productController.getProducts);
router.get("/:id", productController.getProductById);
router.put("/edit/:id", productController.updateProduct);
router.delete('/delete/:id', productController.deleteProduct)



module.exports = router;

