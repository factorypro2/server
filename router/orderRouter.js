const orderController = require("../controller/orderController")
const Router = require("express");
const router = new Router();

router.post("/create", orderController.createOrder);
router.get("/all", orderController.getAllOrders)
router.get("/:id", orderController.getOrderById)
router.put("/changeState", orderController.changeState)

module.exports = router;
