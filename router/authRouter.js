const authController = require('../controller/authController');
const Router = require("express");
const router = new Router();

//Auth routes
router.get("/", authController.getAllUsers);
router.post("/registration", authController.registerUser);
router.post("/login", authController.loginUser);
router.get("/:id", authController.getUserById);
router.get("/username/:username", authController.getUserByUsername);
router.put("/edit/:id", authController.updateUserById);
router.put('/changePassword', authController.changePassword)
router.delete("/delete/:id", authController.deleteUser);

module.exports = router;