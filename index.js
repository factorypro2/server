const express = require("express");
const mongoose = require("mongoose");
const authRouter = require("./router/authRouter");
const permitRouter = require("./router/permitRouter");
const equipmentRouter = require("./router/equipmentRouter");
const productRouter = require("./router/productRouter");
const recordRouter = require("./router/recordRouter");
const orderRouter = require("./router/orderRouter")
const bodyParser = require("body-parser");
const dotenv = require("dotenv").config({ path: "./.env" });
const cors = require("cors");

const app = express();
const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/auth", authRouter);
app.use("/permit", permitRouter)
app.use("/equipment", equipmentRouter)
app.use("/product", productRouter)
app.use("/record", recordRouter)
app.use("/order", orderRouter)


const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI).then(() => {
      console.log("MongoDB connected");
    })
    app.listen(PORT, () => {
      console.log(`\nServer started on port ${PORT}🚀`);
      console.log(`http://localhost:${PORT}`);
    });
  } catch (err) {
    console.log(err);
  }
};

start();