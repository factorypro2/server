const { Schema, model } = require("mongoose");

const OrderId = new Schema({
    lastOrderId: {type: Number, required: true},
})

module.exports = new model("orderId", OrderId);