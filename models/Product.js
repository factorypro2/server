const { Schema, model } = require("mongoose");

const Product = new Schema({
    title: {type: String, required: true},
    quantity: {type: Number, required: true, default: 0},
    measurementUnit: {type: String, required: true},
    records: {type: Array}
})

module.exports = new model("product", Product);