const { Int32 } = require("mongodb");
const { Schema, model } = require("mongoose");

const Equipment = new Schema({
    code: {type: String, required: true},
    title: {type: String, required: true},
    description: {type: String, required: true},
    quantity: {type: Number, required: true}
});

module.exports = new model("equipment", Equipment);