const { ObjectId } = require("mongodb");
const { Schema, model } = require("mongoose");

const Record = new Schema({
    productId: {type: ObjectId, required: true},
    type: {type: String, required: true},
    quantity: {type: Number, required: true, default: 0},
    date: {type: Date, required:true},
    author: {type: String, required: true},
    previousValue: {type: Number, required: true},
    newValue: {type: Number, required: true},
    orderId: {type: String, required: false}
})

module.exports = new model("record", Record);