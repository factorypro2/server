const { Schema, model } = require("mongoose");

const User = new Schema({
  username: { type: String, required: true, unique: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  middleName: { type: String, required: true },
  dateOfBirth: { type: String, required: true, default: "" },
  profileImageUrl: { type: String, required: true, default: "https://static.vecteezy.com/system/resources/thumbnails/005/545/335/small/user-sign-icon-person-symbol-human-avatar-isolated-on-white-backogrund-vector.jpg" },
  password: { type: String, required: true, default: "$2a$10$GEeqbKKD/iVO1CFK.nbZc.7du7H8.bqxE6AVMOcavTv/vqyxjILlS" },
  roles: { type: Array, required: true, default: ['user'] },
  createdAt: { type: Object, default: new Date()},
  lastLogin: { type: Object, default: new Date()},
  token: {type: String, required: false, default: ''}
});

module.exports = new model("user", User);