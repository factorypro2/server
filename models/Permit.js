const { Schema, model } = require("mongoose");

const Permit = new Schema({
    title: {type: String, required: true},
    type: {type: String, required: true},
    typeImageUrl: {type: String, required:false},
    description: {type: String, required: true},
    location: {type: String, required: true},
    startDate: {type: String, required: true},
    endDate: {type: String, required: true},
    numberOfPersons: {type: Number, required: true},
    equipments: {type: Array, required: true},
    signatures: {type: Array},
    state: {type: String, required: true, default: "Draft"},
    userId: {type: String, required: true},
    username: {type: String, required: true}
})

module.exports = new model("permit", Permit);