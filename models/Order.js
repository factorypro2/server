const { Schema, model } = require("mongoose");

const Order = new Schema({
    state: {type: String, required: true},
    orderTitle: {type: String, required: true},
    orderer: {type: String, required: true},
    authorId: {type: String, required: true},
    productTitle: {type: String, required: true},
    quantity: {type: Number, required: true},
    remarks: {type: String, required: false},
    date: {type: Date, required: true},
})

module.exports = new model("order", Order);